from django.urls import path
from accounts.views import render_login, do_logout, render_signup

urlpatterns = [
    path("login/", render_login, name="login"),
    path("logout/", do_logout, name="logout"),
    path("signup/", render_signup, name="signup"),
]
