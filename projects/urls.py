from django.urls import path
from projects.views import (
    render_project,
    render_create_project,
    render_project_list,
)

urlpatterns = [
    path("", render_project_list, name="list_projects"),
    path("<int:id>/", render_project, name="show_project"),
    path("create/", render_create_project, name="create_project"),
]
