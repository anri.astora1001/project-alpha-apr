from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm

# Create your views here.


def render_project_list(request):
    if request.user.is_authenticated:
        projects = Project.objects.filter(owner=request.user)
        context = {"projects": projects}
    else:
        return redirect("login")
    return render(request, "list.html", context)


@login_required
def render_project(request, id):
    project = get_object_or_404(Project, id=id)

    context = {"project": project}
    return render(request, "project.html", context)


@login_required
def render_create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            # project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()
    context = {
        "form": form,
    }
    return render(request, "create_project.html", context)
