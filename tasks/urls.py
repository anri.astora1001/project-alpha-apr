from django.urls import path
from tasks.views import render_create_task, render_my_tasks

urlpatterns = [
    path("create/", render_create_task, name="create_task"),
    path("mine/", render_my_tasks, name="show_my_tasks"),
]
