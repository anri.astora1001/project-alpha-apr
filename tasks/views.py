from django.shortcuts import render, redirect
from tasks.forms import CreateTaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def render_create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            # task = form.save(False)
            # project.owner = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()
    context = {
        "form": form,
    }
    return render(request, "create_project.html", context)


@login_required
def render_my_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user)

    context = {"tasks": my_tasks}
    return render(request, "my_tasks.html", context)
